CREATE DATABASE clipdevops; 

USE clipdevops; 

DROP TABLE IF EXISTS `todos`;
CREATE TABLE `todos` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `owner` varchar(20) NOT NULL,
  `species` varchar(20) NOT NULL,
  `sex` char(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `todos` VALUES (1,'Pablo','Admin','DevOps','M'),(2,'Clip','Admin','Finace','M'),(3,'Clip','Admin','Finace','M'),(4,'Clip','Admin','Finance','M'),(5,'Pay','Admin','Finance','M'),(6,'Pay','QA','Legal','F');
